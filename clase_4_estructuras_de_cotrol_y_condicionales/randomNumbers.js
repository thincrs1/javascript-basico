/**  
Actividad:

Generar un arreglo con números aleatorios entre 1 y 1000
estos números tienen que ser generados con un ciclo y la cantidad 
de elementos se debe pedir mediante un promp

Solicitar un segundo valor con promp y este valor sera un numero base
entre el cual se deberá de condicionar la capacidad de cada numero de ser
divisible entre el numero ingresado, es decir me debe de imprimir todos
los números que sean divisibles entre el número que ingreso el usuario
por medio de los 3 ciclos que vimos hoy, de izquierda a derecha los que cumplan
la condición y de derecha a izquierda los que no cumplan la condición

Bonus hacerlo con un ciclo ambas impresiones, la que cumple y la que no

*/

let randomNumbers = []

const quantityOfRandomNumbers = prompt('Ingresa cuantos números aleatorios quieres')
const min = 1
const max = 1000

let index = 0

while (index < quantityOfRandomNumbers) {
    const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min
    randomNumbers.push(randomNumber)
    index++
}

const factor = prompt('Ingresa el valor que se quiere probar')

for (let i = 0; i < randomNumbers.length; i++) {
    const value = randomNumbers[i]

    if (value % factor === 0) {
        console.log('Este es divisible: ', value)
        console.log('Posición: ', i)
    }
}

for (let i = randomNumbers.length - 1; i > 0; i--) {
    const value = randomNumbers[i]

    if (value % factor !== 0) {
        console.log('Este no es divisible: ', value)
        console.log('Posición: ', i)
    }
}

console.log('Números aleatorios ', randomNumbers)
