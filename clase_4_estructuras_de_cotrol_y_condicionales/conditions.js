
const condition1 = 30
const condition2 = 200
const option = '4'

// IF SIMPLE
if (option !== 'HOLA') {
    console.log('Condición simple')
}

// IF ELSE
if (condition1 <= condition2) {
    // Si la condición se cumple
    console.log('la condición es verdadera')
} else {
    // En caso contrario se ejecuta
    console.log('la condición no se cumple')
}

// IF ELSE IF
if (condition1 < 50) {
    console.log('Se cumple la condición')
} else if (condition1 < 40) {
    console.log('Else if entrando')
}


switch (option) {

    case '1':
        console.log('option 1')
        // en caso de que option valga '1'
        //
        // finalizar la ejecución
        break
    case '2':
        console.log('option 2')
        // en caso de que option valga '2'
        //
        // finalizar la ejecución
        break
    case '3':
        console.log('option 3')
        // en caso de que option valga '3'
        //
        // finalizar la ejecución
        break

    default:
        console.log('default')
        // Equivalente al else
        break

}

// Operador ternario
// equivalente a un if else
// (1 > 10) ? console.log('1 mayor que 10') : console.log('1 no es mayor que 10')
// (1 > 0) && console.log('0 menor que 1')

const number = 6

if (number % 3 === 0) {
    console.log('divisible')
} else {
    console.log('no divisible')
}

switch (number % 3) {

    case 0:
        console.log('case divisble')
        break

    default:
        console.log('case no divisible')
        break
}

(number % 3 === 0) ? console.log('divisible ternario') : console.log('no divisible ternario')

/**  

Tomar de internet un total de 10 numeros aleatorios e ingresarlos aun arreglo entre 1 y 1000
tomar su edad y multiplicarla por un numero aleatorio entre 1 y 10
Hacer una comparativa del valor resultado de su edad con todos los elementos del arreglo
utilizando if else y switch case, hacer lo mismo para if independientes
  
**/

