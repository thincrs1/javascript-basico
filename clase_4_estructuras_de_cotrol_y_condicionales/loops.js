const numbers = [10, 20, 13, 24, 55, 21, 12]

// Loops

// While
let index = 0
// Condición de ejecución
while (index < numbers.length) {
    console.log('Valor ', numbers[index])
    // incremento de iteración
    index++
}
console.log('iteración derecha a izquierda')
index = numbers.length - 1
// Codigo de derecha a izquierda
while (index >= 0) {
    console.log('Valor ', numbers[index])
    index--
}

index = 1
console.log('Saltos dobles')
while (index < numbers.length) {
    console.log('Valor ', numbers[index])
    index += 2
}

console.log('Saltos dobles do while')
index = 1
do {
    console.log('Valor ', numbers[index])
    index += 2
} while (index < numbers.length)

for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] % 2 !== 0) {
        console.log('Pair For: ', numbers[i])
    }
}

index = 0

while (index < numbers.length) {
    if (numbers[index] % 2 === 0) {
        console.log(numbers[index])
    }
    index++
}

const myVariable = prompt('Ingresa el numero de elementos del arreglo')
const min = 0
const max = 1000
const random = Math.floor(Math.random() * (max - min + 1)) + min
alert(random)

/**  
Actividad:

Generar un arreglo con números aleatorios entre 1 y 1000
estos números tienen que ser generados con un ciclo y la cantidad 
de elementos se debe pedir mediante un promp

Solicitar un segundo valor con promp y este valor sera un numero base
entre el cual se deberá de condicionar la capacidad de cada numero de ser
divisible entre el numero ingresado, es decir me debe de imprimir todos
los números que sean divisibles entre el número que ingreso el usuario
por medio de los 3 ciclos que vimos hoy, de izquierda a derecha los que cumplan
la condición y de derecha a izquierda los que no cumplan la condición

Bonus hacerlo con un ciclo ambas impresiones, la que cumple y la que no

*/

