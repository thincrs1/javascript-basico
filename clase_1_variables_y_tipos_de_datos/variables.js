
let myUndefined
let myBoolean = false
let myString = '16'
let myNumber = 1994
let mySecondNumber = 1994
let myBigInt = BigInt(9007199254740991)

console.log('Tipo de dato ', typeof myBoolean)
console.log('Valor de la variable ', myString)
console.log(myNumber)
console.log(myBigInt)

myBoolean = 'No soy boolean'

// Numbers
console.log(' / ', myNumber / mySecondNumber)
console.log(' * ', myNumber * mySecondNumber)
console.log(' - ', myNumber - mySecondNumber)
console.log(' + ', myNumber + mySecondNumber)


