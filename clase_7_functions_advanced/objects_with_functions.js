const student = {
    id: null,
    name: '',
    lastName: '',
    degrees: []
}

const students = []

const getId = () => {
    if (students.length > 0) {
        return students[students.length - 1].id + 1
    }
    return 1000
}

const createStudent = (name, lastName) => {
    const newStudent = { ...student, id: getId(), name, lastName }
    students.push(newStudent)
}

const readStudent = (search) => {
    return students.find((student) => (
        student.id.toString() === search.toString() ||
        student.name.toString().toLowerCase() === search.toString().toLowerCase() ||
        student.lastName.toString().toLowerCase() === search.toString().toLowerCase()
    ))

    let findedStudent = null
    findedStudent = students.find((student) => student.id.toString() === search.toString())
    if (findedStudent) return findedStudent

    findedStudent = students.find((student) => student.name.toString() === search.toString())
    if (findedStudent) return findedStudent

    findedStudent = students.find((student) => student.lastName.toString() === search.toString())
    return findedStudent

    // Optimización
    // n = 3 -> n es el numero de estudiantes

    // Caso 1
    // Comparaciones - 11
    // Iteraciones - 9
    // Uso de memoria - 1 estudiante adicional

    // Comparaciones - 9
    // Iteraciones - 3
    // Uso de memoria - No necesita memoria adicional
}

const updateStudent = (id, prop, val) => {
    students[id][prop] = val
}

const deleteStudent = (id) => {
    const studenToDelete = readStudent(id)
    const indexToDelete = students.indexOf(studenToDelete)
    students.splice(indexToDelete, 1)
}

const getAllStudents = () => {
    return students
}

let option

do {
    option = prompt(`
        [1] Crear un nuevo estudiante
        [2] Obtener datos de un estudiante
        [3] Actualizar un estudiante
        [4] Eliminar un estudiante
        [5] Traer todos los estudiantes
        [6] Salir     
        `
    )
    switch (option) {
        case '1':
            const name = prompt('Ingresa el nombre')
            const lastName = prompt('Ingresa el apellido')
            createStudent(name, lastName)
            break
        case '2':
            const search = prompt('Ingresa el ID, Nombre o Apellido a buscar')
            const findedStudent = readStudent(search)
            if (findedStudent) {
                alert('El estudiante es ' + findedStudent.name + ' ' + findedStudent.lastName)
            } else {
                alert('No se encontró el estudiante')
            }
            break

        case '3':
            const idToUpdate = prompt('Ingresa el ID a buscar')
            const prop = prompt('Ingresa el campo que deseas cambiar')
            const val = prompt('Ingresa el valor nuevo')
            updateStudent(idToUpdate, prop, val)
        case '4':
            const idToDelete = prompt('Ingresa el id que deseas eliminar')
            const confirmed = confirm('Confirmas que deseas eliminar el id' + idToDelete)
            if (confirmed) {
                deleteStudent(idToDelete)
            }
        case '5':
            console.log('Todos los estudiantes :', getAllStudents())
    }
} while (option !== '6')

/**
1 - Validen que en la función update, número uno busque al usuario y en caso que no
exista no pedir los demas valores e informar al usuario con un alert que el usuario
no existe 

2 - Si la propiedad que me está enviando el usuario no corresponde ni a name ni a lastName
debe de informarme que la propiedad no existe y regresarme al menú principal
**/
