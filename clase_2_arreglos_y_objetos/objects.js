let student = {
    name: 'Victor',
    age: 29,
    id: 9300311,
    career: 'Cibernetica',
    isActive: true,
    degrees: ['Math', 'Algorithms', 'IA'],
    scores: [{
        name: 'Math',
        semester: 1,
        score: 100
    }, {
        name: 'Math',
        semester: 4,
        score: 90
    }, {
        name: 'Algorithnms',
        semester: 5,
        score: 80
    }]
}

console.log(student.name)
console.log(student.scores[1])
console.log(Object.keys(student))
console.log(student['id'])
console.log(Object.values(student))

student.name = 'Jose'

console.log(student.name)

// 0x425
let a = 10
// 0x530
let b = a
a = 30
console.log(a, b)

// 0x120
let test = { name: 'Objeto de prueba' }
// 0x124
let test2 = { ...test }
console.log(test, test2)
test2.name = 'Esta es una segunda prueba'
console.log(test, test2)

let arr = [1, 2]
let arr2 = [...arr]

arr2.pop()

console.log(arr, arr2)

// Crear un objeto persona
// Asignarle una propiedad al menos de cada tipo de dato primitivo
// Asignarle un arreglo al menos de tipo de dato a elección
// Probar ambos metodos vistos .keys .values
// Reasignar todas las propiedades del arreglo utilizando corchetes
// Volver a mandar llamar ambos métodos .keys y .value

