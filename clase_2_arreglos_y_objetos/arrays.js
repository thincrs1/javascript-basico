
let alumno1 = 'Christian Romo'
let alumno2 = 'Pau'

// Arreglos
let students = ['Christian Romo', 'Pau', 'Pau', 'Pau', 'Pau', 'Pau', 'Pau', 'Pau',]

students.push('Victor', 'Jairo', 'Ariel')

let numbers = [1, 2, 3, 4, 5, 6, 7]

console.log(typeof numbers[0])

// Definir un arreglo con un tamaño específico
// al arreglo definido le vas a ingresar los nombres de todos
// los estudiantes de la sesión
let myCustomArray = new Array(3)
myCustomArray[0] = 'Antonio'
myCustomArray[1] = 'Arturo'
myCustomArray[2] = 'Christian'
console.log(myCustomArray)


// Definir un arreglo con tamaño dinámico
// ingresar datos de cualquier tipo de dato primirivo que desees
// diferente de string y asignarle tantos valores a dicho arreglo
// como sea tu edad y eliminar después tantos elementos como sea
// tu mes de nacimiento
let anotherArray = []
anotherArray.push(1, 2, 3)

console.log(anotherArray)

anotherArray.pop()

console.log(anotherArray)