const container = document.getElementById('container')

container.style = 'background-color: #eee; padding: 4em'

const calculatorButtons = [7, 8, 9, '/', 4, 5, 6, '*', 1, 2, 3, '-', 0, 'C', '=', '+']
const buttonsArray = []

let firstValue = ''
let secondValue = ''
let operator = ''
let activeOperator = null

const buttonStyle = 'font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: white; border-radius: 5px'
const hoverStyle = 'cursor:pointer; font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: tian; border-radius: 5px'
const activeStyle = 'font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: #ABDBE3; border-radius: 5px'

const calculate = () => {
    const input = document.getElementById('result')
    switch (operator) {
        case '+':
            input.value = parseInt(firstValue) + parseInt(secondValue)
            break
        case '-':
            input.value = parseInt(firstValue) - parseInt(secondValue)
            break
        case '*':
            input.value = parseInt(firstValue) * parseInt(secondValue)
            break
        case '/':
            input.value = parseInt(firstValue) / parseInt(secondValue)
            break
    }
}

const setValuesToCalculate = (val) => {
    const input = document.getElementById('result')
    const button = document.getElementById('btn' + val)

    if (Number.isInteger(val)) {
        if (operator) {
            secondValue += val.toString()
            input.value = secondValue
        } else {
            firstValue += val.toString()
            input.value = firstValue
        }
    } else {
        switch (val) {
            case '=':
                const selectedOperator = document.getElementById('btn' + activeOperator)
                selectedOperator.style = buttonStyle
                activeOperator = null
                calculate()
                break
            case 'C':
                firstValue = ''
                secondValue = ''
                operator = ''
                input.value = ''
                break
            default:
                if (firstValue) {
                    if (activeOperator) {
                        const lastButton = document.getElementById('btn' + activeOperator)
                        lastButton.style = buttonStyle
                    }
                    button.style = activeStyle
                    activeOperator = val
                    operator = val
                    input.value = '0'
                }
        }
    }
}

document.addEventListener('keyup', (event) => {
    const presedEvent = calculatorButtons.find(el => el.toString() === event.key)

    if (presedEvent !== undefined) setValuesToCalculate(presedEvent)
    if (event.key === 'Enter') setValuesToCalculate('=')
    if (event.key === 'Backspace') setValuesToCalculate('C')
})

calculatorButtons.forEach((el, index) => {
    const button = document.createElement('button')
    button.textContent = el
    button.style = buttonStyle
    button.addEventListener('mouseenter', () => activeOperator !== el ? button.style = hoverStyle : null)
    button.addEventListener('mouseleave', () => activeOperator !== el ? button.style = buttonStyle : null)
    button.addEventListener('click', () => setValuesToCalculate(el))
    button.id = 'btn' + el
    buttonsArray.push(button)

    if ((index + 1) % 4 === 0) {
        const br = document.createElement('br')
        buttonsArray.push(br)
    }
})

const result = document.createElement('input')
result.placeholder = ''
result.style = "margin-top: 5em; width: 100%; padding: 10px; border-color: transparent"
result.id = 'result'
buttonsArray.push(result)

buttonsArray.forEach(btn => container.append(btn))


/*
Tomar la calculadora actual, despues de que se agregue un restulado o que se calcule
si yo presiono otro operador debe tomar el valor resultado anterior como el valor
inicial de calculo para una segunda operación, ingreso el segundo valor y entonces
me recalcula

Solucionar el problema que tiene la calculadora cuando no hay ningún valor seleccionado
y presiono un operador, no debe de permitirlo

Si se seleccionan  multiples operadores, que se quite el anterior y se ponga el nuevo
*/