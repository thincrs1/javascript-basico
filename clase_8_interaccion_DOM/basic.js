
const myButton = document.getElementById('btnHiWorld')
const myH1 = document.getElementById('title')
const myList = document.getElementById('myList')
const myInput = document.getElementById('input')

const elements = []


myH1.addEventListener('click', () => {
    myH1.style = 'color: red'
})

myH1.addEventListener('mouseenter', () => {
    myH1.style = 'color: green; cursor: pointer'
})

myH1.addEventListener('mouseleave', () => {
    myH1.style = 'color: blue'
})

myButton.addEventListener('click', () => {
    const val = myInput.value
    console.log('input', val)
    addElementToList()
})

const getRandomArbitrary = (min, max) => {
    return Math.random() * (max - min) + min;
}

const addElementToList = () => {
    elements.push(getRandomArbitrary(1, 1000))

    myList.innerHTML = ''
    elements.forEach(el => {
        const li = document.createElement('li')
        li.append(Math.round(el))
        myList.append(li)
    })
}