
/**
const myFunction = (num, res, err) => {

    if (isNaN(num)) {
        return err('El elemento ingresado no es un número')
    }

    return res(num * 50)
}

const myNumber = parseInt(prompt('Ingresa el número que desees'))

myFunction(myNumber, (data) => {
    alert(data)
}, (err) => {
    alert('Error: ' + err)
})

*/

/** 
const myForEach = (arr, cb) => {
    for (let i = 0; i < arr.length; i++) {
        cb(arr[i], i, arr[i] * 2)
    }
}

const myArr = [1, 2, 3, 4]

myForEach(myArr, (el, index, mul) => {
    console.log('elemento', el, mul)
})

*/

const container = document.getElementById('container')
const list = document.getElementById('list')
const btnPokemons = document.getElementById('btnPokemons')
const btnNext = document.getElementById('btnNext')
const btnPrev = document.getElementById('btnPrev')

let prevUrl = ''
let nextUrl = ''

const getPokemons = (defaultUrl) => {
    return new Promise(async (resolve, reject) => {
        try {
            const url = defaultUrl ? defaultUrl : 'https://pokeapi.co/api/v2/pokemon/'
            const res = await axios.get(url)
            resolve(res.data)
        } catch (err) {
            reject(err)
        }
    })
}

const setPokemons = async (url) => {
    try {
        const res = await getPokemons(url)

        prevUrl = res.previous
        nextUrl = res.next

        list.innerHTML = ''
        res.results.forEach(el => {
            const myLi = document.createElement('li')
            myLi.innerHTML = 'Pokemon: ' + el.name
            list.appendChild(myLi)
        })
    } catch (err) {
        console.log('Error al obtener pokemons ', err)
    }
}

btnPokemons.addEventListener('click', async () => {
    await setPokemons()
})

btnNext.addEventListener('click', () => {
    setPokemons(nextUrl)
})

btnPrev.addEventListener('click', () => {
    setPokemons(prevUrl)
})


/** 

Tomar el ejercicio actual de obtención de pokemons y cuando yo le de click a un pokemon
determinado debo de mostrar al menos 5 propiedades del pokemon que clickee en un contenedor 
debajo de mi HTML actual, hay que recordar que para obtener la url del pokemon es en base al
nombre del mismo, es decir el detalle es la siguiente URL

https://pokeapi.co/api/v2/pokemon/ditto -> donde ditto es el nombre del pokemon

*/