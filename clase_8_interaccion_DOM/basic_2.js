const myHovers = document.getElementsByClassName('myHover')
const btnContainer = document.getElementById('btnContainer')

Array.from(myHovers).forEach(element => {
    element.addEventListener('mouseenter', () => element.style = 'color: red; cursor: pointer')
    element.addEventListener('mouseleave', () => element.style = 'color: black')
})

const numberOfButtons = 50
const buttonsArray = []

for (let i = 0; i < numberOfButtons; i++) {
    const button = document.createElement('button')
    button.textContent = 'Botón ' + (i + 1)
    button.style = 'margin-right: 10px; margin-top: 10px'
    button.addEventListener('click', () => alert(i + 1))
    buttonsArray.push(button)
}

buttonsArray.forEach(btn => btnContainer.append(btn))