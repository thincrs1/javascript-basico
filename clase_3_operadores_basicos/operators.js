// Operadores airtméticos

// + - * /
// % ++ -- 
let number = 1
let aux = 2

number = number + aux
console.log(number)

number = number - aux
console.log(number)

number = number * aux
console.log(number)

number = 452 / 13
console.log('Division ', number)

// Validación de si un número es par o impar
number = 12
number = number % 2
console.log('is pair ', number)

number = 5

number++
console.log(number)
number--
console.log(number)

// Operadores de asignación

// Asignación simple =
// Asignación con operadores += -= *= /= %=

number = 10

number += 10
console.log(number)

number -= 5
console.log(number)

number *= 2
console.log(number)

number /= 5
console.log(number)

number %= 2
console.log(number)

// Operadores Comparación

/** 
Imaginemos que estos no existen
// Igual a                  ==
// Diferente de             !=
*/

// Igual a (estricto)       ===
// Diferente de (estricto)  !==
// < > <= >=

console.log(1 >= 1)

// Operadores Lógicos

// AND &&
// OR ||
// Negación !

console.log(1 === 1 && 2 > 4) // false
console.log(1 === 1 || 2 > 4) // true
console.log(!(1 === 1 && 2 > 4)) // true
console.log(!(1 === 1 || 2 > 4)) // false


let data = [31, 3]
let results = []
// Primer operador es el mas +

results.push('+')
results.push((data[0] + data[1]).toString())
console.log('Results ', results)

results.push('-')
results.push((data[0] - data[1]).toString())
console.log('Results ', results)