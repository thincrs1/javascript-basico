const calculator = (operator, val1, val2) => {
    switch (operator) {
        case '+':
            return parseFloat(val1) + parseFloat(val2)
        case '-':
            return parseFloat(val1) - parseFloat(val2)
        case '*':
            return parseFloat(val1) * parseFloat(val2)
        case '/':
            return parseFloat(val1) / parseFloat(val2)

        default:
            return 'no se reconoce el operador'
    }
}

const main = () => {

    let option

    do {
        option = prompt('[1] Calculador \n[5] Salir')

        // En caso de que el usuario escoja 1 se manda a llamar la función
        // calculadora
        if (option === '1') {
            const operator = prompt('Ingresa el operador')
            const val1 = prompt('Ingresa el valor 1')
            const val2 = prompt('Ingresa el valor 2')
            const result = calculator(operator, val1, val2)
            alert('La calculadora dio: ' + result)
        }

    } while (option !== '5')

    alert('Muchas gracias por haber usado el mejor programa del mundo !!!!')
}

main()


/**
Completar el programa visto en clase con opciones en el menú desde 
el 1 hasta el 4 donde cada número es una función diferente y la 
opción 5 es para salir del programa, las funciones deberán corresponder
a códigos o explicaciones que se vieron durante la clase
*/
