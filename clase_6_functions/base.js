// Función registro tipo arrow
const register = () => {
    // Cuerpo de la función
}

// Función normal
function createTweete() {
    // Cuerpo de la función
}

// Funcionalidad de este bloque es que
// le mandemos un arreglo de numeros y 
// nos regrese los numeros al doble
const mutiplier = (data, factor) => {
    const results = []
    for (let i = 0; i < data.length; i++) {
        results.push(data[i] * factor)
    }
    return results
}

const data = [1, 2, 3]
const myDoubles = mutiplier(data, 2)
const myTriples = mutiplier(data, 3)

/** 
Generar una iteración del 1 al 10 en la cual se ha de llamar
la función con el indice de iteración actual y se deberá imprimir
en consola el resultado

punto importante el índice 0 no se va a imprimir, si no que será
del 1 en adelante hasta llegar al 10 incluyendolo
*/

for (let i = 0; i < 10; i++) {
    const result = mutiplier(data, i + 1)
    console.log('Usando la función ', result)
}

