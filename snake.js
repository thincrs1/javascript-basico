const food = document.getElementById('food')
const board = document.getElementById('board')
const foodPosition = {
    left: 0,
    top: 0
}

const snake = []
let endGame = false
let direction = 'right'
let newDirection = ''

const getRandomPosition = () => {
    const max = 19
    const min = 0

    return Math.round(Math.random() * (max - min) + min) * 40
}

const setFoodPosition = () => {
    const x = getRandomPosition()
    const y = getRandomPosition()

    food.style = 'left: ' + x + 'px; top: ' + y + 'px;'
    foodPosition.left = x
    foodPosition.top = y
}

const eatFood = () => {

    let newSnake = document.createElement('div')
    newSnake.className = 'snake'
    board.append(newSnake)
    snake.push({
        element: newSnake,
        left: 0,
        top: 0
    })
    setFoodPosition()
}

const moveSnake = () => {

    const oldDirection = direction
    direction = newDirection

    if (newDirection === 'left' && oldDirection === 'right') direction = 'right'
    if (newDirection === 'right' && oldDirection === 'left') direction = 'left'
    if (newDirection === 'top' && oldDirection === 'bottom') direction = 'bottom'
    if (newDirection === 'bottom' && oldDirection === 'top') direction = 'top'

    const auxSnake = [...snake].reverse()

    auxSnake.forEach((part, index) => {
        if (auxSnake[index + 1]) {
            const nextPart = auxSnake[index + 1]
            part.left = nextPart.left
            part.top = nextPart.top
            part.element.style = 'left: ' + part.left + 'px; top: ' + part.top + 'px;'
        }
    })

    let head = snake[0]
    let el = head.element
    switch (direction) {
        case 'right':
            head.left += 40
            break
        case 'left':
            head.left -= 40
            break
        case 'bottom':
            head.top += 40
            break
        case 'top':
            head.top -= 40
            break
    }
    el.style = 'left: ' + head.left + 'px; top: ' + head.top + 'px;'

    if (head.left === foodPosition.left && head.top === foodPosition.top) {
        eatFood()
    }
}

const game = () => {

    setFoodPosition()
    const interval = setInterval(() => {
        if (!endGame) {
            moveSnake()
        } else {
            clearInterval(interval)
        }
    }, 100)

}

document.addEventListener('keyup', (e) => {
    if (e.code === 'ArrowDown') newDirection = 'bottom'
    if (e.code === 'ArrowUp') newDirection = 'top'
    if (e.code === 'ArrowLeft') newDirection = 'left'
    if (e.code === 'ArrowRight') newDirection = 'right'
    if (e.code === 'Enter') game()
})

const initialSnake = document.createElement('div')
initialSnake.className = 'snake'
board.append(initialSnake)
snake.push({
    element: initialSnake,
    left: 0,
    top: 0
})

